package pl.sda.javalub13;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class HelloWorldGeneratorTest {

    @Test
    public void sayHello() {
    //given:
       HelloWorldGenerator hwg = new HelloWorldGenerator();
       String testName = "testName";
    //when:
    String result = hwg.sayHello(testName);
    //then:
        Assert.assertEquals("Hello testName, and have a nice day.",result);

    }
}