package pl.sda.javalub13.tictactoe;

import org.junit.Test;
import pl.sda.javalub13.tictactoe.game.engine.GameBoard;
import pl.sda.javalub13.tictactoe.game.engine.Move;
import pl.sda.javalub13.tictactoe.game.engine.Player;
import pl.sda.javalub13.tictactoe.game.engine.exception.InvalidMoveCoordinatesException;

public class GameBoardTest {

    @Test
    public void makeMove() {
        Player p1 = new Player("Name1", 'o');
        Player p2 = new Player("Name2", 'x');
        GameBoard gb = new GameBoard(p1, p2);

        try {
            boolean m1 = gb.makeMove(new Move('A', 3));
            boolean m2 = gb.makeMove(new Move('b', 1));
            boolean m3 = gb.makeMove(new Move('b', 3));
            boolean m4 = gb.makeMove(new Move('b', 2));
            boolean m5 = gb.makeMove(new Move('c', 3));
        } catch (InvalidMoveCoordinatesException e) {
            e.printStackTrace();
        }


        System.out.println();

    }
}