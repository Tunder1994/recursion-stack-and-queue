package pl.sda.javalub13.data.structures.queue;

import org.junit.Test;

import java.util.LinkedList;
import java.util.Queue;

public class SdaQueueImplTest {

    @Test
    public void get() {
    }

    @Test
    public void add() {
        SdaQueueImpl<String> queue = new SdaQueueImpl<>();
        queue.offer("first");
        queue.offer("second");
        queue.offer("third");
        queue.offer("fourth");

        int size3 = queue.size3();

        queue.remove("first");

        String pick1 = queue.poll();
        String pick2 = queue.poll();
        String pick3 = queue.poll();
        String pick4 = queue.poll();
        String pick5 = queue.poll();


        System.out.println();

    }

    @Test
    public void javaQueueExample() {
        Queue<String> javaStdQueue = newQueue();
        javaStdQueue.offer("first");
        javaStdQueue.offer("second");
        javaStdQueue.offer("third");
        javaStdQueue.offer("fourth");

        String pick1 = javaStdQueue.poll();
        String pick2 = javaStdQueue.poll();
        String pick3 = javaStdQueue.poll();
        String pick4 = javaStdQueue.poll();
        String pick5 = javaStdQueue.poll();
    }

    private Queue<String> newQueue() {
        return new LinkedList<>();
    }
}