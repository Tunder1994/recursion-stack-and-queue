package pl.sda.javalub13.data.structures.stack;

import org.junit.Test;

import java.util.Deque;
import java.util.Stack;

import static org.junit.Assert.assertEquals;

public class SdaStackImplTest {

    @Test
    public void push() {
        SdaStack<String> stack = new SdaStackImpl<>();
        stack.push("1");
        stack.push("2");
        stack.push("3");

        assertEquals(3, stack.size());
    }

    @Test
    public void pop() {
        SdaStack<String> stack = new SdaStackImpl<>();
        stack.push("1");
        stack.push("2");
        stack.push("3");
        String pop1 = stack.pop();
        String pop2 = stack.pop();
        String pop3 = stack.pop();

        assertEquals("3", pop1);
        assertEquals("2", pop2);
        assertEquals("1", pop3);



    }

    @Test
    public void peek() {
        SdaStack<String> stack = new SdaStackImpl<>();
        stack.push("1");
        stack.push("2");
        stack.push("3");
        String peek1 = stack.peek();
        String peek2 = stack.peek();

        assertEquals("3",peek1);
        assertEquals("3",peek2);
    }
}