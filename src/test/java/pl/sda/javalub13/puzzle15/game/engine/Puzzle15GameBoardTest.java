package pl.sda.javalub13.puzzle15.game.engine;

import org.junit.Test;

import static org.junit.Assert.*;

public class Puzzle15GameBoardTest {

    @Test
    public void test(){
        Puzzle15GameBoard gb = new Puzzle15GameBoard(null, new MyRandomizer());
        gb.move('0');
        gb.move('3');
        System.out.println();
    }

}
class MyRandomizer extends GameFieldRandomizer{
    @Override
    void randomize(GameFields gameFields, int numberOfRounds) {
        super.randomize(gameFields, 5);
    }
}
