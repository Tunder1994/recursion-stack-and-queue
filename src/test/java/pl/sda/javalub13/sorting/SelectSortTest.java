package pl.sda.javalub13.sorting;

import org.junit.Test;

import static org.junit.Assert.*;

public class SelectSortTest {

    @Test
    public void sort() {
        SelectSort selectSort = new SelectSort();
        int[] tab = {4,6,9,1,5,0,12,16};
        selectSort.sort(tab);
        assertArrayEquals(new int[]{0,1,4,5,6,9,12,16},tab);
    }
}