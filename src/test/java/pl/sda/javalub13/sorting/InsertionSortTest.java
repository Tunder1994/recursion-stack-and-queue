package pl.sda.javalub13.sorting;

import org.junit.Test;

import static org.junit.Assert.*;

public class InsertionSortTest {

    @Test
    public void sort() {
        InsertionSort sort = new InsertionSort();
        int[] tab1 = {4,6,9,1,5,0,12,16};
        int[] tab2 = {4,6,9,1,5,0,12,16};
        sort.sort(tab1);
        sort.sort2(tab2);
        assertArrayEquals(new int[]{0,1,4,5,6,9,12,16},tab1);
        assertArrayEquals(new int[]{0,1,4,5,6,9,12,16},tab2);
    }
}