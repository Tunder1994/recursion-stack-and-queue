package pl.sda.javalub13.sorting;

import org.junit.Test;

import static org.junit.Assert.*;

public class QuickSortTest {

    @Test
    public void sort() {
        QuickSort quickSort = new QuickSort();
        int[] tab = {4,6,9,1,5,0,12,16,-1};
        quickSort.sort(tab);
        assertArrayEquals(new int[]{-1,0,1,4,5,6,9,12,16},tab);
    }
}