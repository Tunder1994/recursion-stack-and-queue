package pl.sda.javalub13.sorting;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BubbleSortTest {

    @Test
    public void sort() {
        BubbleSort bs = new BubbleSort();
        int[] tab = {6,3,8,1,0,2};
        bs.sort(tab);

        Assert.assertArrayEquals(new int[]{0,1,2,3,6,8},tab);
    }
}