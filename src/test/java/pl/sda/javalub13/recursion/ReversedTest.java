package pl.sda.javalub13.recursion;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ReversedTest {

    @Test
    public void reverseEmpty() {
        //given:
        Reversed reversed = new Reversed();
        String emptyString = "";
        // when:
        String result = reversed.reverse(emptyString);
        //then:
        Assert.assertEquals("",result);
    }
    @Test
    public void reverseOneCharLength() {
    //given:
        Reversed reversed = new Reversed();
        String oneChar = "A";
    // when:
        String result = reversed.reverse(oneChar);
    //then:
        Assert.assertEquals("A",result);
    }
    @Test
    public void reverseText() {
        //given:
        Reversed reversed = new Reversed();
        String oneChar = "Ala ma kota";
        // when:
        String result = reversed.reverse(oneChar);
        //then:
        Assert.assertEquals("atok am alA",result);
    }
}