package pl.sda.javalub13.recursion;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class StrongTest {

    @Test
    public void factorial1() {
        Strong strong = new Strong();
        long result = strong.factorial(0);
        Assert.assertEquals(1,result);
    }

    @Test
    public void factorial2() {
        Strong strong = new Strong();
        long result = strong.factorial(1);
        Assert.assertEquals(1,result);
    }
    @Test
    public void factorial3() {
        Strong strong = new Strong();
        long result = strong.factorial(9);
        Assert.assertEquals(362880,result);
    }
}