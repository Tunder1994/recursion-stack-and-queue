package pl.sda.javalub13.recursion;

import org.junit.Assert;
import org.junit.Test;

public class PowerTest {

    @Test
    public void pow1() {
        Power power = new Power();
        long result = power.pow(2,0);
        Assert.assertEquals(1,result);
    }

    @Test
    public void pow2() {
        Power power = new Power();
        long result = power.pow(2,1);
        Assert.assertEquals(2,result);
    }

    @Test
    public void pow3() {
        Power power = new Power();
        long result = power.pow(2,5);
        Assert.assertEquals(32,result);
    }
}