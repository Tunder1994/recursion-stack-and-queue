package pl.sda.javalub13.recursion;

import org.junit.Test;

import static org.junit.Assert.*;

public class FibonacciTest {

    @Test
    public void shouldReturnZeroForElementZero() {
        //given:
        Fibonacci fibonacci = new Fibonacci();
        int element = 0;

        //when:
        int result = fibonacci.fib(element);
        //then:
        assertEquals(0,result);

    }

    @Test
    public void shouldReturnZeroForNegativeElement() {
        //given:
        Fibonacci fibonacci = new Fibonacci();
        int element = -3;

        //when:
        int result = fibonacci.fib(element);
        //then:
        assertEquals(0,result);

    }

    @Test
    public void shouldCalculateFib9() {
        //given:
        Fibonacci fibonacci = new Fibonacci();
        int element = 9;

        //when:
        int result = fibonacci.fib(element);
        //then:
        assertEquals(34,result);

    }
}