package pl.sda.javalub13.tictactoe.game.engine;

public interface WinConditions {

    boolean isWinner(char[][] fields, char playerSign);
}
