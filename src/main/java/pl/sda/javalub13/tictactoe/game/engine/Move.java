package pl.sda.javalub13.tictactoe.game.engine;

import pl.sda.javalub13.tictactoe.game.engine.exception.InvalidMoveCoordinatesException;

public class Move {
    private final char column;
    private final int row;

    private static final String allowedColumns = "abcABC";


    public Move(char column, int row) throws InvalidMoveCoordinatesException {
        if (allowedColumns.contains(String.valueOf(column)) && row > 0 && row < 4) {
            this.column = column;
            this.row = row;
        } else {
            throw new InvalidMoveCoordinatesException(String.format("Invalid move coordinates! Allowed columns: %s, allowed rows [1,2,3]",allowedColumns));
        }
    }

    public int columnIndex() {
        switch (column) {
            case 'A':
            case 'a':
                return 0;
            case 'B':
            case 'b':
                return 1;
            case 'C':
            case 'c':
                return 2;
            default:
                throw new IllegalStateException("Invalid move coordinates!");
        }
    }

    public int rowIndex() {
        return row - 1;
    }
}
