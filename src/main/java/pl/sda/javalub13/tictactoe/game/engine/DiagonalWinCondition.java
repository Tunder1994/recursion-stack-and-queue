package pl.sda.javalub13.tictactoe.game.engine;

public class DiagonalWinCondition implements WinConditions {
    @Override
    public boolean isWinner(char[][] gameFields, char playerSign) {
            int countDiagonal1 = 0;
            int countDiagonal2 = 0;
            for (int i = 0; i < gameFields.length; i++) {
                if (gameFields[i][i] == playerSign) {
                    countDiagonal1++;
                }
                if (gameFields[i][2 - i] == playerSign) {
                    countDiagonal2++;
                }
            }
            if (countDiagonal1 == gameFields.length || countDiagonal2 == gameFields.length) {
                return true;
            }
            return false;

    }
}
