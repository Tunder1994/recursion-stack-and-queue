package pl.sda.javalub13.tictactoe.game.engine;

public class Player {
    private final String name;
    private final char sign;

    public Player(String name, char sign) {
        this.name = name;
        this.sign = sign;
    }

    public String name() {
        return name;
    }

    public char sign() {
        return sign;
    }
}
