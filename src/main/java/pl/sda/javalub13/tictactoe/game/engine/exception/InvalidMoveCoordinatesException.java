package pl.sda.javalub13.tictactoe.game.engine.exception;

public class InvalidMoveCoordinatesException extends Exception {

    public InvalidMoveCoordinatesException(String message) {
        super(message);
    }
}
