package pl.sda.javalub13.tictactoe.game.engine;

import java.util.*;

import static java.util.Arrays.asList;

public class GameBoard {
    public static final char EMPTY_FIELD = ' ';
    protected final char[][] gameFields = {
            {EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD},
            {EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD},
            {EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD}
    };
    List<WinConditions> winCondition = Arrays.asList(new RowOrColumnWinCondition(), new DiagonalWinCondition());
    private Player currnetPlayer;
    private final Queue<Player> gameQueue;


    public GameBoard(Player player1, Player player2) {
        this.gameQueue = new LinkedList<>(asList(player1, player2));
        nextPlayer();
    }

    private void nextPlayer() {
        currnetPlayer = gameQueue.poll();
        gameQueue.offer(currnetPlayer);
    }

    public boolean makeMove(Move move) {
        int col = move.columnIndex();
        int row = move.rowIndex();

        if (gameFields[row][col] == EMPTY_FIELD) {
            gameFields[row][col] = currnetPlayer.sign();
            if(!isWinner().isPresent()) {
                nextPlayer();
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean gameBoardContainsEmptyFields(){
        for (int i=0; i < gameFields.length ; i++){
            for (int j=0; j < gameFields.length; j++){
                if(gameFields[i][j] == EMPTY_FIELD){
                    return true;
                }
            }
        }
        return false;
    }

    public Optional <Player> isWinner() {
        return winCondition.stream().map(winCondition -> winCondition.isWinner(gameFields, currnetPlayer.sign()))
                .filter(result -> result == true).findAny().map(success -> currnetPlayer);

    }



    public char[][] getGameFields() {
        return gameFields;
    }



}
