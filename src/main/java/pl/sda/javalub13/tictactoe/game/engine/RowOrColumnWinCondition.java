package pl.sda.javalub13.tictactoe.game.engine;

public class RowOrColumnWinCondition implements WinConditions {
    @Override
    public boolean isWinner(char[][] gameFields, char playerSign) {

            for (int j = 0; j < 3; j++) {
                int rowCount = 0;
                int colCount = 0;
                for (int i = 0; i < gameFields.length; i++) {
                    if (gameFields[j][i] == playerSign) {
                        rowCount++;
                    }
                    if (gameFields[i][j] == playerSign) {
                        colCount++;
                    }
                }
                if (rowCount == gameFields.length || colCount == gameFields.length) {
                    return true;
                }

            }
            return false;
        }
    }

