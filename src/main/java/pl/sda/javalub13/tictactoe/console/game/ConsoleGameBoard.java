package pl.sda.javalub13.tictactoe.console.game;

import pl.sda.javalub13.tictactoe.game.engine.GameBoard;
import pl.sda.javalub13.tictactoe.game.engine.Player;

public class ConsoleGameBoard extends GameBoard {

    public ConsoleGameBoard(Player player1, Player player2) {
        super(player1, player2);
    }

    public void print(){
        System.out.println("    A   B   C");
        for(int i =0 ; i < getGameFields().length; i++){
            System.out.print(i+1 + " |");
            for (int j=0; j < getGameFields().length; j++){
                System.out.printf(" %s |", gameFields[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }
}
