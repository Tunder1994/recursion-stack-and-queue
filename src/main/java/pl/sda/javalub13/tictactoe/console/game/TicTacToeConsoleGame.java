package pl.sda.javalub13.tictactoe.console.game;

import pl.sda.javalub13.tictactoe.game.engine.Move;
import pl.sda.javalub13.tictactoe.game.engine.Player;
import pl.sda.javalub13.tictactoe.game.engine.exception.InvalidMoveCoordinatesException;

import java.util.Scanner;

public class TicTacToeConsoleGame {

    public static void main(String[] args) {
        Player player1 = null;
        Player player2 = null;

        Menu menu = new Menu();
        do {
            Menu.MenuOption userChoice = menu.display();

            switch (userChoice) {
                case NEW_GAME: {
                    newGameHandle(player1, player2);
                    break;
                }
                case ENTER_PLAYERS: {
                    player1 = loadPlayer('x');
                    player2 = loadPlayer('o');
                    break;
                }
                case DISPAY_RANKING: {
                    //todo display ranking
                    break;
                }
                case EXIT: {
                    System.exit(0);
                }
            }
        } while (true);

    }

    private static void newGameHandle(Player player1, Player player2) {
        if (player1 == null || player2 == null) {
            System.out.println("Please provide players before starting game");
            return;
        } else {
            play(player1, player2);
        }
    }

    private static void play(Player player1, Player player2) {
        Scanner scanner = new Scanner(System.in);
        ConsoleGameBoard consoleGameBoard = new ConsoleGameBoard(player1, player2);
        boolean moveSuccess = false;
        do {
            moveSuccess = false;
            consoleGameBoard.print();
            System.out.println("Please enter next coordinates");
            String moveAsString = scanner.next();
            char column = moveAsString.charAt(0);
            int row = Integer.parseInt(moveAsString.substring(1, 2));
            Move move = null;
            try {
                move = new Move(column, row);
                moveSuccess = consoleGameBoard.makeMove(move);
            } catch (InvalidMoveCoordinatesException e) {
                System.out.println(e.getMessage());
            }



        } while ((!moveSuccess || !consoleGameBoard.isWinner().isPresent()) && consoleGameBoard.gameBoardContainsEmptyFields());
        consoleGameBoard.print();
        if(consoleGameBoard.isWinner().isPresent()){
            System.out.printf("Congratulations! %s is winner", consoleGameBoard.isWinner().get().name());
            System.out.println();
        }else {
            System.out.println("DRAW!!!");
        }



    }

    private static Player loadPlayer(char playerSign) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter name: ");
        String playerName = scanner.next();
        System.out.println(String.format("Player %s loaded successfully.", playerName));
        return new Player(playerName, playerSign);
    }
}
