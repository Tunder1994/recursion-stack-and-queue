package pl.sda.javalub13.tictactoe.console.game;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import static pl.sda.javalub13.tictactoe.console.game.Menu.MenuOption.*;

public class Menu {

    public MenuOption display() {
        Scanner scanner = new Scanner(System.in);

        List<Integer> allowedOptions = Arrays.stream(MenuOption.values()).map(MenuOption::getPositioninMenu).collect(Collectors.toList());
        int userChoice;

        do {
            System.out.println("--------------------------------------");
//            System.out.println(NEW_GAME);
//            System.out.println(DISPAY_RANKING);
//            System.out.println(ENTER_PLAYERS);
//            System.out.println(EXIT);
            Arrays.stream(MenuOption.values()).forEach(System.out::println);

//            for(MenuOption option :MenuOption.values()){
//            System.out.println(option);
//            }

            userChoice = scanner.nextInt();


        } while (!allowedOptions.contains(userChoice));
        final int userOption = userChoice;
        return Arrays.stream(MenuOption.values()).filter(allowedOption -> allowedOption.getPositioninMenu() == userOption).findFirst()
                .orElseThrow(() -> new IllegalStateException("Should never happen"));
    }

    public enum MenuOption {
        NEW_GAME(1, "Start new game"),
        DISPAY_RANKING(2, "Display global ranking"),
        ENTER_PLAYERS(3, "Enter players names"),
        EXIT(4, "Exit Game");


        private int positioninMenu;
        private final String msg;

        MenuOption(int positionInMenu, String msg) {
            this.positioninMenu = positionInMenu;
            this.msg = msg;
        }

        public int getPositioninMenu() {
            return positioninMenu;
        }

        public String getDisplayName() {
            return msg;
        }

        public String toString() {
            return String.format("%d -> %s", positioninMenu, msg);
        }
    }
}
