package pl.sda.javalub13.tictactoe;

import pl.sda.javalub13.tictactoe.console.game.ConsoleGameBoard;
import pl.sda.javalub13.tictactoe.game.engine.Player;

public class Main {
    public static void main(String[] args) {
        Player p1 = new Player("Name1", 'o');
        Player p2 = new Player("Name2", 'x');

        ConsoleGameBoard consoleGameBoard = new ConsoleGameBoard(p1,p2);
        consoleGameBoard.print();
    }
}
