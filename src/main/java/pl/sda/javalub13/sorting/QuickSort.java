package pl.sda.javalub13.sorting;


public class QuickSort {

    public void sort(int[] tab) {
        sort(tab, 0, tab.length - 1);
    }

    private void sort(int[] tab, int startIdx, int endIdx) {
        if (startIdx < endIdx) {
            int partitioningPoint = partition(tab, startIdx, endIdx);
            sort(tab, startIdx, partitioningPoint);
            sort(tab, partitioningPoint + 1, endIdx);
        }
    }

    private int partition(int[] tab, int startIdx, int endIdx) {
        int minIdx = (startIdx + endIdx) / 2;
        int pivot = tab[minIdx];

        int i = startIdx - 1;
        int j = endIdx + 1;

        while (i < j) {
            do {
                i++;
            } while (tab[i] < pivot);
            do {
                j--;
            } while (tab[j] > pivot);
            if (i <= j) {
                return j;
            }
            swap(tab, i, j);
        }
        return j;
    }

    private void swap(int[] tab, int i, int j) {
        int tmp = tab[i];
        tab[i] = tab[j];
        tab[j] = tmp;
    }
}
