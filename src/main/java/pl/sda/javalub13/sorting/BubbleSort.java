package pl.sda.javalub13.sorting;

public class BubbleSort {

    public void sort(int[] tab) {
        boolean swapped = true;
        int length = tab.length;
        while (swapped) {
            swapped = false;
            for (int i = 1; i < length; i++) {
                if(tab[i-1] > tab[i]){
                    swap(tab, i-1, i);
                    swapped =true;
                }
            }
            length--;
        }

    }

    private void swap(int[] tab, int i1, int i2) {
        int tmp = tab[i1];
        tab[i1] = tab[i2];
        tab[i2] = tmp;
    }
}
