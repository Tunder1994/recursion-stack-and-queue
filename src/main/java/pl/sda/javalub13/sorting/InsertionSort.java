package pl.sda.javalub13.sorting;

public class InsertionSort {

    public void sort(int[] tab) {

        for (int i = 1; i < tab.length; i++) {
            int currnet = tab[i];
            int j = i - 1;
            boolean swapped =false;
            while (j >= 0 && tab[j] > currnet) {
                tab[j+1] = tab[j];
                j--;
                swapped = true;
            }
            if(swapped) {
                tab[j + 1] = currnet;
            }
        }
    }
    public void sort2(int[] tab) {

        for (int i = 1; i < tab.length; i++) {
            int j = i - 1;
            while (j >= 0 && tab[j] > tab[j+1]) {
                swap(tab,j,j+1);
                j--;

            }
        }
    }

    private void swap(int[] tab, int i, int j) {
        int tmp = tab[i];
        tab[i] = tab[j];
        tab[j] = tmp;
    }

}

