package pl.sda.javalub13.sorting;

public class SelectSort {

    public void sort (int[] tab){
        for(int i=0; i<tab.length-1; i++){
            int min_idx =i;
            for(int j=i+1; j< tab.length;j++){
                if(tab[j] < tab [min_idx]){
                    min_idx = j;
                }

            }
            int temp = tab[min_idx];
            tab[min_idx] = tab[i];
            tab[i] = temp;
        }
    }
}
