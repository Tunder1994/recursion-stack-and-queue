package pl.sda.javalub13;

public class HelloWorldGenerator {

    public String sayHello(String name){
        return String.format("Hello %s, and have a nice day.", name);
    }
}
