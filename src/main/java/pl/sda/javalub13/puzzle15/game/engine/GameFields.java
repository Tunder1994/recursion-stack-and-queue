package pl.sda.javalub13.puzzle15.game.engine;

public class GameFields {
    private int emptyField;
    private int[][] fields;
    private Coordinates emptyFieldsCoordinates;


    public GameFields() {
        fields = new int[4][4];
        emptyField = 0;
        int counter = 1;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                fields[i][j] = counter;
                counter++;
            }
        }
        fields[3][3] = emptyField;
        emptyFieldsCoordinates = new Coordinates(new Row(3), new Column(3));
    }
    public boolean push(Coordinates c2) {
        if(c2.isAdjacentTo(emptyFieldsCoordinates)) {
            int c1Col = emptyFieldsCoordinates.getCol().colNumber();
            int c1Row = emptyFieldsCoordinates.getRow().rowNumber();
            int c2Col = c2.getCol().colNumber();
            int c2Row = c2.getRow().rowNumber();
            int tmp = fields[c1Row][c1Col];
            fields[c1Row][c1Col] = fields[c2Row][c2Col];
            fields[c2Row][c2Col] = tmp;
            emptyFieldsCoordinates = c2;
            return true;
        }else {
            return false;
        }

    }

    public Coordinates emptyFieldsCoordinates() {
        return emptyFieldsCoordinates;
    }

    public boolean isSorted() {
        String result = "";
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                result += fields[i][j] + ",";
            }
        }

        return ("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0").equals(result);
    }
}
