package pl.sda.javalub13.puzzle15.game.engine;

public class Move15Puzzle {

    private char move;

    public Move15Puzzle(char move) {
        this.move = move;
    }


    public boolean isVertical() {
        return Character.isDigit(move);
    }

    public Row getRow(){
        return new Row(Character.digit(move,10));
    }

    public Column getColumn() {
        return new Column("abcd".indexOf(Character.toLowerCase(move)));
    }
}
