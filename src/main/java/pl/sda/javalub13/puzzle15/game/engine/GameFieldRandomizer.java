package pl.sda.javalub13.puzzle15.game.engine;

import java.util.Random;

public class GameFieldRandomizer {

    void randomize(GameFields gameFields, int numberOfRounds) {
        Random random = new Random();
        int counter = 1;
        boolean moved = false;
        do {
            int direction = random.nextInt(4) + 1;

            switch (direction) {
                case 1: {
                    Boolean moveSuccess = gameFields.emptyFieldsCoordinates().upper().map(gameFields::push).orElse(false);
                    if (moveSuccess) {
                        counter++;
                    }
                    break;
                }
                case 2: {
                    Boolean moveSuccess = gameFields.emptyFieldsCoordinates().right().map(gameFields::push).orElse(false);
                    if (moveSuccess) {
                        counter++;
                    }
                    break;
                }
                case 3: {
                    Boolean moveSuccess = gameFields.emptyFieldsCoordinates().lower().map(gameFields::push).orElse(false);
                    if (moveSuccess) {
                        counter++;
                    }
                    break;
                }
                case 4: {
                    Boolean moveSuccess = gameFields.emptyFieldsCoordinates().left().map(gameFields::push).orElse(false);
                    if (moveSuccess) {
                        counter++;
                    }
                    break;
                }
                default:
                    throw new IllegalStateException("Should never happen");
            }
        } while (counter < numberOfRounds);

    }
}