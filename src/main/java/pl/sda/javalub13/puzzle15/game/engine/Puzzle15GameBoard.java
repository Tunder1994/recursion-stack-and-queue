package pl.sda.javalub13.puzzle15.game.engine;


import pl.sda.javalub13.tictactoe.game.engine.Move;

import java.util.Optional;

public class Puzzle15GameBoard {
    protected GameFields gameFields;
    Player player;


    public Puzzle15GameBoard(Player player, GameFieldRandomizer gameFieldRandomizer) {
        this.gameFields = new GameFields();
        this.player = player;
        gameFieldRandomizer.randomize(gameFields,1000);
    }

    public boolean isSolved() {
        return gameFields.isSorted();
    }

    public boolean move(char rawMove) {
        Move15Puzzle move = new Move15Puzzle(rawMove);
        if (move.isVertical()) {
            return makeVerticalMove(move);
        } else {
            return makeHorizontalMove(move);
        }
    }

    private boolean makeHorizontalMove(Move15Puzzle move) {
        Column colToBeMoved = move.getColumn();
        if (colToBeMoved.isValidColumn()) {
            if (colToBeMoved.isAfter(gameFields.emptyFieldsCoordinates().getCol())) {
                do {
                    gameFields.emptyFieldsCoordinates()
                            .right()
                            .map(right -> gameFields.push(right));
                } while (!gameFields.emptyFieldsCoordinates().getCol().equals(colToBeMoved));
                return true;
            } else if (colToBeMoved.isBefore(gameFields.emptyFieldsCoordinates().getCol())) {
                do {
                    gameFields.emptyFieldsCoordinates()
                            .left()
                            .map(left -> gameFields.push(left));
                } while (!gameFields.emptyFieldsCoordinates().getCol().equals(colToBeMoved));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean makeVerticalMove(Move15Puzzle move) {
        Row rowToBeMoved = move.getRow();
        if (rowToBeMoved.isValidRow()) {
            if (rowToBeMoved.isBelow(gameFields.emptyFieldsCoordinates().getRow())) {
                do {
                    gameFields.emptyFieldsCoordinates()
                            .upper()
                            .map(upper -> gameFields.push(upper));
                } while (!gameFields.emptyFieldsCoordinates().getRow().equals(rowToBeMoved));
                return true;
            } else if (rowToBeMoved.isAbove(gameFields.emptyFieldsCoordinates().getRow())) {
                do {
                    gameFields.emptyFieldsCoordinates().lower()
                            .map(lower -> gameFields.push(lower));
                } while (!gameFields.emptyFieldsCoordinates().getRow().equals(rowToBeMoved));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

