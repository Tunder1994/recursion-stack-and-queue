package pl.sda.javalub13.puzzle15.game.engine;

public class Row {
    private int rowNumber;

    public Row(int rowNumber) {

        this.rowNumber = rowNumber;
    }


    public boolean isValidRow() {
        return  rowNumber>-1 && rowNumber <4;
    }

    public boolean isBelow(Row otherRow) {
        return rowNumber > otherRow.rowNumber;
    }

    public boolean isAbove(Row otherRow) {
        return rowNumber < otherRow.rowNumber;
    }

    public int rowNumber() {
        return rowNumber;
    }

    public boolean equals(Row otherRow) {
        return rowNumber == otherRow.rowNumber;
    }
}
