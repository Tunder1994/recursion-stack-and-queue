package pl.sda.javalub13.puzzle15.game.engine;

public class Column {
    private int columnNumer;

    public Column(int columnNumer) {
        this.columnNumer = columnNumer;
    }

    public boolean isValidColumn() {
        return columnNumer >-1 && columnNumer< 4;
    }

    public boolean isAfter(Column otherColumn) {
        return columnNumer > otherColumn.columnNumer;
    }

    public int colNumber() {
        return columnNumer;
    }

    public boolean isBefore(Column otherColumn) {
        return columnNumer < otherColumn.columnNumer;
    }

    public boolean equals(Column otherColumn) {
        return columnNumer == otherColumn.columnNumer;
    }
}
