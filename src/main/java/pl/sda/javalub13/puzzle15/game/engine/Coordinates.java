package pl.sda.javalub13.puzzle15.game.engine;

import java.util.Optional;

import static java.lang.Math.abs;

public class Coordinates {
    private Row row;
    private Column col;

    public Coordinates(Row row, Column col) {

        if (row.isValidRow() && col.isValidColumn()) {
            this.row = row;
            this.col = col;
        } else {
            throw new IllegalStateException("Invalid coordinates!");
        }
    }

    public Row getRow() {
        return row;
    }

    public Column getCol() {
        return col;
    }

    public Optional<Coordinates> lower() {
        Row lowerColumnNumber = new Row(row.rowNumber() - 1);
        return Optional.of(lowerColumnNumber).filter(Row::isValidRow).map(row1 -> new Coordinates(row1, col));
    }

    public Optional<Coordinates> upper() {
        Row upperRowNumber = new Row(row.rowNumber() + 1);
        return Optional.of(upperRowNumber).filter(Row::isValidRow).map(row1 -> new Coordinates(row1, col));
    }

    public Optional<Coordinates> right() {
        Column rightColumnNumber = new Column(col.colNumber() + 1);
        return Optional.of(rightColumnNumber).filter(Column::isValidColumn).map(column -> new Coordinates(row, column));
    }

    public Optional<Coordinates> left() {
        Column leftColumnNumber = new Column(col.colNumber() - 1);
        return Optional.of(leftColumnNumber).filter(Column::isValidColumn).map(column -> new Coordinates(row, column));
    }

    public boolean isAdjacentTo(Coordinates other) {
        int thisColNumber = col.colNumber();
        int thisRowNumber = row.rowNumber();
        int otherColNumber = other.col.colNumber();
        int otherRowNumber = other.row.rowNumber();
        if ((thisColNumber == otherColNumber) && (abs(thisRowNumber - otherRowNumber) == 1)) {
            return true;
        } else if ((thisRowNumber == otherRowNumber) && (abs(thisColNumber - otherColNumber) == 1)) {
            return true;
        } else {
            return false;
        }
    }
}
