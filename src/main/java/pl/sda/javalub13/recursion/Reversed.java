package pl.sda.javalub13.recursion;

public class Reversed {
    public String reverse(String text){
        if(text.length()<= 1){
            return text;
        }else {
            char lastChar = text.charAt(text.length() - 1);
            String textWithoutCharacter = text.substring(0,text.length()-1);
            return lastChar + reverse(textWithoutCharacter);

        }

    }
}
