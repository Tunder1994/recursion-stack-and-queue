package pl.sda.javalub13.recursion;

public class Power {
    long pow(int base, int exponet) {
        if (exponet == 0) {
            return 1;
        } else {
            return base * pow(base, exponet - 1);
        }

    }
}
