package pl.sda.javalub13.recursion;

public class Strong {

    public long factorial(int base){
        if(base == 0 || base ==1){
            return 1;
        }else {
            return base * factorial(base-1);
        }

    }
}
