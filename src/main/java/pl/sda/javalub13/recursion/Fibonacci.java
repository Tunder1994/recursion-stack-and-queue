package pl.sda.javalub13.recursion;

public class Fibonacci {

    public int  fib(int i){
        if(i<0){
            return 0;
        }
        if (i == 1 || i == 0 ){
            return i;
        }else{
            return fib(i-1) + fib(i-2);
        }
    }
}
