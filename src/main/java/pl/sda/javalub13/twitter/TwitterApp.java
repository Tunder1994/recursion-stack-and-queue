package pl.sda.javalub13.twitter;

import pl.sda.javalub13.twitter.repository.*;
import pl.sda.javalub13.twitter.service.TweetService;
import pl.sda.javalub13.twitter.service.UserAlreadyExists;
import pl.sda.javalub13.twitter.service.UserService;

import java.util.List;

public class TwitterApp {

    public static void main(String[] args) throws UserAlreadyExists {
        UserRepository userRepository = new InMemoryUserRepository();
        UserService userService = new UserService(userRepository);
        TweetRepository tweetRepository = new InMemoryTweetRepository();
        NotificationRepository notificationRepository = new InMemoryNotificationRepository();
        TweetService tweetService = new TweetService(userRepository,tweetRepository, notificationRepository);
        UserId rosa = userService.createUser(new Password("Abcdefg1"), "drosa6", "email@email.com");
        UserId mazurek = userService.createUser(new Password("Qwertyas1"), "gmazurek", "emai@daw.com");

        userService.follow(rosa, mazurek);

        tweetService.tweet(mazurek, new Tweet("Hello Twitter!"));

        List<Notification> notificationList = notificationRepository.pendingNotifications(rosa);

        notificationList.stream().map(notification -> notification.message(userRepository));
        System.out.println();

    }
}
