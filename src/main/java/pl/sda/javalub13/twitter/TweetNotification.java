package pl.sda.javalub13.twitter;

import pl.sda.javalub13.twitter.repository.UserRepository;

import static java.lang.String.format;

public class TweetNotification implements Notification {
    private final UserId tweetAuthor;
    private final UserId target;
    private final TweetId subject;


    public TweetNotification(UserId tweetAuthor, UserId target, TweetId subject) {
        this.tweetAuthor = tweetAuthor;
        this.target = target;
        this.subject = subject;
    }

    @Override
    public String message(UserRepository userRepository) {
        User author = userRepository.find(tweetAuthor);
        return format("Hi! User %s published new tweet %s",author.getNickname(), subject.getId());
    }

    @Override
    public UserId target() {
        return target;
    }
}
