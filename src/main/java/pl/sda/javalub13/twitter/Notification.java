package pl.sda.javalub13.twitter;

import pl.sda.javalub13.twitter.repository.UserRepository;

public interface Notification {

    String message(UserRepository userRepository);
    UserId target();

}
