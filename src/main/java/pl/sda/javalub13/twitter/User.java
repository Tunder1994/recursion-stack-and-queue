package pl.sda.javalub13.twitter;

import java.util.Arrays;

public class User {
    private final Password password;
    private final String nickname;
    private final String email;

    public User(Password password, String nickname, String email) {
        this.password = password;
        this.nickname = nickname;
        this.email = email;
    }

    public Password getPassword() {
        return password;
    }

    public String getNickname() {
        return nickname;
    }

    public String getEmail() {
        return email;
    }
}
