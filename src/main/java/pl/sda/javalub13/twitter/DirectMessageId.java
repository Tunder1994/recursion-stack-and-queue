package pl.sda.javalub13.twitter;

import java.util.UUID;

public class DirectMessageId {

    private final String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }
}
