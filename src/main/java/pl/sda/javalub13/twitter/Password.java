package pl.sda.javalub13.twitter;

import java.util.Arrays;

public class Password {

    private final String password;

    public Password(String password) {
        if (password.length() < 8) {
            throw new IllegalStateException("Password Length invalid");
        }
        boolean containsDigit = false;
        boolean containsLowerCase = false;
        boolean containsUpperCase = false;
        for (char c : password.toCharArray()) {
            if(Character.isDigit(c)){
                containsDigit = true;
            }
            if(Character.isLowerCase(c)){
                containsLowerCase = true;
            }
            if(Character.isUpperCase(c)){
                containsUpperCase = true;
            }
        }

        if(!containsDigit){
            throw new IllegalStateException("Password must contain at least one digit");
        }
        if(!containsLowerCase){
            throw new IllegalStateException("Password must contain at least one lower case letter");
        }
        if(!containsUpperCase){
            throw new IllegalStateException("Password must contain at least one upper case letter");
        }


        this.password = password;
    }


}
