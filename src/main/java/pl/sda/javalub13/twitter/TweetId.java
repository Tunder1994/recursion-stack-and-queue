package pl.sda.javalub13.twitter;

public class TweetId {

    private final String id;

    public TweetId(String id) {

        this.id = id;
    }

    public String getId() {
        return id;
    }
}
