package pl.sda.javalub13.twitter;

public class DirectMessage {
    private final DirectMessage message;
    private final UserId sender;
    private final UserId recevier;



    public DirectMessage(UserId sender, UserId recevier, DirectMessage message) {

        this.sender = sender;
        this.recevier = recevier;
        this.message = message;
    }
}
