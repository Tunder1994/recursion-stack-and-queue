package pl.sda.javalub13.twitter.repository;

import pl.sda.javalub13.twitter.Notification;
import pl.sda.javalub13.twitter.UserId;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InMemoryNotificationRepository implements NotificationRepository {

    List<Notification> notifications = new ArrayList<>();

    @Override
    public void save(Notification notification) {
        notifications.add(notification);
    }

    @Override
    public List<Notification> pendingNotifications(UserId user) {
        return notifications.stream().filter(notification -> notification.target().equals(user)).collect(Collectors.toList());
    }
}
