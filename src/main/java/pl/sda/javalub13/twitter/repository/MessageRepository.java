package pl.sda.javalub13.twitter.repository;

import pl.sda.javalub13.twitter.DirectMessage;
import pl.sda.javalub13.twitter.DirectMessageId;

public interface MessageRepository {

    DirectMessageId save(DirectMessage message);
}
