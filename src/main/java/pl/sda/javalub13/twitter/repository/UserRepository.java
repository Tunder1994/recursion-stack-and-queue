package pl.sda.javalub13.twitter.repository;

import pl.sda.javalub13.twitter.User;
import pl.sda.javalub13.twitter.UserId;

import java.util.List;

public interface UserRepository {
    UserId create(User newUser) ;

    boolean checkIfExist(String email);

    void follow(UserId follower, UserId followed);

    List<UserId> getMyFollowers(UserId user);

    User find(UserId userId);
}
