package pl.sda.javalub13.twitter.repository;

import pl.sda.javalub13.twitter.Tweet;
import pl.sda.javalub13.twitter.TweetId;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class InMemoryTweetRepository implements TweetRepository {

    Map<TweetId, Tweet> tweets = new HashMap<>();

    @Override
    public TweetId save(Tweet tweet) {
        TweetId id = new TweetId(UUID.randomUUID().toString());
        tweets.put(id, tweet);
        return id;
    }
}
