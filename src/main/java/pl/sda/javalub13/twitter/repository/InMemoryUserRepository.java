package pl.sda.javalub13.twitter.repository;

import pl.sda.javalub13.twitter.User;
import pl.sda.javalub13.twitter.UserId;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryUserRepository implements UserRepository {
    private final Map<UserId, User> users = new HashMap<>();
    private final List<Follow> follows = new ArrayList<>();

    @Override
    public UserId create(User newUser) {
        UserId uid = new UserId(UUID.randomUUID().toString());
        users.put(uid, newUser);
        return uid;
    }

    @Override
    public boolean checkIfExist(String email) {
        return users.entrySet().stream().anyMatch(user -> user.getValue().getEmail().equalsIgnoreCase(email));
    }

    @Override
    public void follow(UserId follower, UserId followed) {
        follows.add(new Follow(follower,followed));
    }

    @Override
    public List<UserId> getMyFollowers(UserId user) {
        List<UserId> followers = follows.stream().filter(follow -> follow.followed.equals(user)).map(follow -> follow.getFollower()).collect(Collectors.toList());
        return  followers;
    }

    @Override
    public User find(UserId userId) {
        return  users.get(userId);
    }

    private  class Follow{
        private final UserId follower;
        private  final UserId followed;

        private Follow(UserId follower, UserId followed) {
            this.follower = follower;
            this.followed = followed;
        }

        public UserId getFollower() {
            return follower;
        }

        public UserId getFollowed() {
            return followed;
        }
    }
}
