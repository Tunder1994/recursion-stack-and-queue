package pl.sda.javalub13.twitter.repository;

import pl.sda.javalub13.twitter.DirectMessage;
import pl.sda.javalub13.twitter.DirectMessageId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryMessageRepository implements MessageRepository {

    private final Map<DirectMessageId, DirectMessage> messages = new HashMap<>();

    @Override
    public DirectMessageId save(DirectMessage message) {
        DirectMessageId msdId = new DirectMessageId();
        messages.put(msdId, message);
        return msdId;
    }
}
