package pl.sda.javalub13.twitter.repository;

import pl.sda.javalub13.twitter.Notification;
import pl.sda.javalub13.twitter.TweetId;
import pl.sda.javalub13.twitter.UserId;

import java.util.List;

public interface NotificationRepository {
    void save(Notification notification);

    List<Notification> pendingNotifications(UserId userId);
}
