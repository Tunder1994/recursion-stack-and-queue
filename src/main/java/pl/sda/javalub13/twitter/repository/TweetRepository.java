package pl.sda.javalub13.twitter.repository;

import pl.sda.javalub13.twitter.Tweet;
import pl.sda.javalub13.twitter.TweetId;

public interface TweetRepository {
   TweetId save(Tweet tweet);
}
