package pl.sda.javalub13.twitter.service;

import pl.sda.javalub13.twitter.Password;
import pl.sda.javalub13.twitter.User;
import pl.sda.javalub13.twitter.UserId;
import pl.sda.javalub13.twitter.repository.UserRepository;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public UserId createUser(Password password, String nickname, String email) throws UserAlreadyExists {
        boolean userExists = userRepository.checkIfExist(email);
        if(!userExists){
        User newUser = new User(password, nickname, email);
        UserId userId = userRepository.create(newUser);
        return userId;
        }else throw new UserAlreadyExists();
    }

    public void follow(UserId follower, UserId followed){
        //sprawdzenie biznesowe czy mozna wykonac operacjie
        // np. czy nie wykorzystalem limitu sledzonych osob lub
        // czy nie jestem na czarnej liscie przez tego użytkownika
        userRepository.follow(follower, followed);
    }
}
