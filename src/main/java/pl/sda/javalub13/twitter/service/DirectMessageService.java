package pl.sda.javalub13.twitter.service;

import pl.sda.javalub13.twitter.*;
import pl.sda.javalub13.twitter.repository.MessageRepository;
import pl.sda.javalub13.twitter.repository.NotificationRepository;

public class DirectMessageService {

    private final MessageRepository messageRepository;
    private final NotificationRepository notificationRepository;

    public DirectMessageService(MessageRepository messageRepository, NotificationRepository notificationRepository) {

        this.messageRepository = messageRepository;
        this.notificationRepository = notificationRepository;
    }

    public DirectMessageId directMessage(UserId sender, UserId recevier, DirectMessage message) {
        DirectMessage directMessage = new DirectMessage(sender, recevier, message);
        DirectMessageId directMessageId = messageRepository.save(directMessage);
        Notification msgNotification = new DirectMessageNotification(recevier, sender, directMessageId);
        notificationRepository.save(msgNotification);
        return directMessageId;
    }
}
