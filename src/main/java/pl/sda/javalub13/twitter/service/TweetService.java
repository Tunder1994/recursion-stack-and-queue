package pl.sda.javalub13.twitter.service;

import pl.sda.javalub13.twitter.*;
import pl.sda.javalub13.twitter.repository.NotificationRepository;
import pl.sda.javalub13.twitter.repository.TweetRepository;
import pl.sda.javalub13.twitter.repository.UserRepository;

import java.util.List;

public class TweetService {
    private final UserRepository userRepository;
    private final TweetRepository tweetRepository;
    private final NotificationRepository notificationRepository;

    public TweetService(UserRepository userRepository, TweetRepository tweetRepository, NotificationRepository notificationRepository) {
        this.userRepository = userRepository;
        this.tweetRepository = tweetRepository;
        this.notificationRepository = notificationRepository;
    }

    public TweetId tweet(UserId author, Tweet tweet) {
        TweetId tweetId = tweetRepository.save(tweet);
        List<UserId> followers = userRepository.getMyFollowers(author);
        followers.stream().forEach(follower -> {
            Notification notification = new TweetNotification(author, follower, tweetId);
            notificationRepository.save(notification);
        });
        return tweetId;
    }
}
