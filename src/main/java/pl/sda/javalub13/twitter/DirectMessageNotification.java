package pl.sda.javalub13.twitter;

import pl.sda.javalub13.twitter.repository.UserRepository;

public class DirectMessageNotification implements Notification {
    private final UserId target;
    private final UserId author;
    private final DirectMessageId subject;

    public DirectMessageNotification(UserId target, UserId author, DirectMessageId subject) {
        this.target = target;
        this.author = author;
        this.subject = subject;
    }

    @Override
    public String message(UserRepository userRepository) {
        User sender = userRepository.find(author);
        return String.format("New message from %s", sender.getNickname());
    }

    @Override
    public UserId target() {
        return target;
    }


}
