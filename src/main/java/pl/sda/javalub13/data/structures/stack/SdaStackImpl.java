package pl.sda.javalub13.data.structures.stack;

public class SdaStackImpl<T> implements SdaStack<T> {

    Node<T> top = null;
    private int size=0;

    @Override
    public void push(T data) {


        size++;
        if(top == null){

            top = new Node<T>(data);
        }else {
            Node<T> newNode= new Node<>(data,top);
            top = newNode;
        }
    }

    @Override
    public T pop() {
        if(top != null){
            size--;
            Node<T> oldTop = top;
            top=top.next();
            return oldTop.getData();
        }else {
            return null;
        }

    }

    @Override
    public T peek() {
        if(top!=null){
            return top.getData();
        }else {
            return null;
        }


    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        top = null;
        size= 0;
    }

    private class Node<T> {
        private final T data;
        private final Node<T> next;

        Node(T data) {
            this.data = data;
            this.next = null;
        }

        Node(T data, Node<T> next) {
            this.data = data;
            this.next = next;
        }

        T getData(){
            return data;
        }

        Node<T> next(){
            return next;
        }

        boolean hasNext(){
            return next != null;
        }
    }
}
