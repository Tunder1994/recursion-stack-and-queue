package pl.sda.javalub13.data.structures.list;

public interface SdaList<T> {

    void add(T data);
    void add(int idx, T data);

    T get(int idx);

    void remove(int idx);

    void clear();

    int size();

    boolean contains(T data);
}
