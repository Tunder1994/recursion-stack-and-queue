package pl.sda.javalub13.data.structures.list;

public class SdaLinkedList<T> implements SdaList<T> {
    Node<T> head = null;
    Node<T> tail = null;

    @Override
    public void add(T data) {
        if (tail == null) {
            Node<T> node = new Node<>(data);
            head = node;
            tail = node;
        } else {
            Node<T> node = new Node<>(data, tail);
            tail.setNext(node);
            tail = node;
        }

    }

    @Override
    public void add(int idx, T data) {
        if (idx == 0) {
            Node<T> node = new Node<>(data, null, head);
            head = node;
        } else if (idx >= size()) {
            add(data);
        } else {
            Node<T> searchedNode = gettNode(idx);
            Node<T> searchedPrev = searchedNode.prev;
            Node<T> node = new Node<>(data, searchedPrev, searchedNode);
            searchedPrev.setNext(node);
            searchedNode.setPrev(node);

        }
    }

    @Override
    public T get(int idx) {
        Node<T> searchedNode = gettNode(idx);
        return searchedNode.getData();
    }

    private Node<T> gettNode(int idx) {
        Node<T> searchedNode;
        if (idx < size() / 2) {
            searchedNode = head;
            int tmpIdx = 0;
            while (tmpIdx < idx) {
                searchedNode = searchedNode.next;
                tmpIdx++;
            }
        } else {
            searchedNode = tail;
            int tmpIdx = size() - 1;
            while (tmpIdx > idx) {
                searchedNode = searchedNode.prev;
                tmpIdx--;
            }
        }
        return searchedNode;
    }

    @Override
    public void remove(int idx) {
        if (idx == 0) {
            head = head.next;
            head.setPrev(null);
        } else if (idx >= size() - 1) {
            tail = tail.prev;
            tail.setNext(null);
        } else {
        }
    }

    @Override
    public void clear() {
        head = null;
        tail = null;
    }

    @Override
    public int size() {
        if (head == null) {
            return 0;
        } else {
            int size = 1;
            Node<T> tmpNode = head;
            while (tmpNode.hasNext()) {
                size++;
                tmpNode = tmpNode.next;
            }
            return size;
        }
    }

    @Override
    public boolean contains(T data) {
        Node<T> searchedNode;
        searchedNode = head;
        while (data != searchedNode.getData()){
            searchedNode = searchedNode.next();
            if(searchedNode.next()==null){
                return false;
            }
        }
        return true;
    }

    private class Node<T> {
        private T data;
        private Node<T> next;
        private Node<T> prev;

        Node(T data) {
            this.data = data;
            next = null;
            prev = null;
        }

        Node(T data, Node<T> prev) {
            this.data = data;
            this.prev = prev;
            next = null;
        }

        Node(T data, Node<T> prev, Node<T> next) {
            this.data = data;
            this.prev = prev;
            this.next = next;
        }

        public void setPrev(Node<T> prev) {
            this.prev = prev;
        }

        public void setNext(Node<T> next) {
            this.next = next;
        }

        Node<T> next() {
            return next;
        }

        Node<T> prev() {
            return prev;
        }

        public T getData() {
            return data;
        }

        public boolean hasNext() {
            return next != null;
        }
    }

}
