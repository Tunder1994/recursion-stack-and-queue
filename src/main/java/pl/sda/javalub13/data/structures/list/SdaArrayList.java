package pl.sda.javalub13.data.structures.list;

import java.util.Arrays;

public class SdaArrayList<T> implements SdaList<T> {

    T[] dataArray = (T[]) new Object[1];
    int size = 0;

    @Override
    public void add(T data) {
        if(size == dataArray.length){
            extendArray(data);
        }
        dataArray[size] = data;
        size++;

    }

    private T extendArray(T data) {
        int newLength = dataArray.length +1;
        dataArray = Arrays.copyOf(dataArray,newLength);
        dataArray[dataArray.length -1] = data;
        return (T) dataArray;
    }

    @Override
    public void add(int idx, T data) {
        if(idx >= size()){
            add(data);
        }else {
            if(idx>=dataArray.length){
                extendArray(data);
            }
            for (int i = size; i > idx; i--) {
                dataArray[i] = dataArray[i - 1];
            }
            dataArray[idx] = data;
            size++;
        }
    }

    @Override
    public T get(int idx) {
        if(idx >=size()){
            return null;
        }else {
            return dataArray[idx-1];
        }
    }

    @Override
    public void remove(int idx) {
        if (idx >= size) return;
        if (idx == size - 1) {
            dataArray[idx] = null;
        } else {
            for (int i = idx; i <= size; i++) {
                dataArray[i] = dataArray[i+1];
            }
        }
        size--;
    }

    @Override
    public void clear() {
        dataArray = (T[]) new Object[1];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean contains(T data) {
        for(T searchedData : dataArray){
            if(searchedData == null) {
                break;
            } else if (searchedData.equals(data)){
                return true;
            }
        }
        return false;
    }
}
