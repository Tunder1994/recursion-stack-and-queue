package pl.sda.javalub13.data.structures.stack;

public interface SdaStack<T> {

    /**
     * Adds new element to this stack
     * @param data to be added on top of this stack, data cannot be null.
     */
    void push(T data);

    /**
     * Retrieves and remoces top elemet from this stack
     * @return current stack element, null if empty.
     */
    T pop();

    /**
     * Looks at the object at the top of this stack without removing it from the stack
     */
    T peek();

    /**
     * @return returns numer of elements on this stack
     */
    int size();

    void clear();
}
