package pl.sda.javalub13.data.structures.bst;

import java.util.Optional;

public class BinarySearchTree {

    Node root = null;

    public Optional<Integer> min() {
        if (root != null) {
            Node maxNode = max(root);
            return Optional.of(min(root).value);
        } else {
            return Optional.empty();
        }
    }

    private Node min(Node startingPoint) {
        Node tmpNode = startingPoint;
        while (tmpNode.hasLeft()) {
            tmpNode = tmpNode.left;
        }
        return tmpNode;
    }

    public Optional<Integer> max() {
        if (root != null) {
            Node maxNode = max(root);
            return Optional.of(max(root).value);
        } else {
            return Optional.empty();
        }
    }

    private Node max(Node startingPoint) {

        Node tmpNode = startingPoint;
        while (tmpNode.hasRight()) {
            tmpNode = tmpNode.right;
        }
        return tmpNode;
    }

    public void add(int value) {
        if (root == null) {
            root = new Node(value);
        } else {
            Node insertionPoint = locateInserionPoint(root, value);
            insertionPoint.addChild(value);
        }
    }

    public Node locateInserionPoint(Node startingPoint, int value) {
        if (value < startingPoint.value) {
            // bede szukcac z lewej strony drzrwa
            if (startingPoint.left == null) {
                return startingPoint;
            } else {
                return locateInserionPoint(startingPoint.left, value);
            }

        } else {
            // bede szukac z prawej strony drzewa
            if (startingPoint.right == null) {
                return startingPoint;
            } else {
                return locateInserionPoint(startingPoint.right, value);
            }
        }
    }

    public boolean contains(int value) {

        return true;
    }

    Optional<Node> find(Node startingPoint, int value) {
        if (startingPoint == null) {
            return Optional.empty();
        } else {
            if (startingPoint.value == value) {
                return Optional.of(startingPoint);
            } else {
                if (value < startingPoint.value) {
                    return find(startingPoint.right, value);
                } else {
                    return find(startingPoint.left, value);
                }
            }
        }
    }

    public void remove(int value) {
        Optional<Node> valueToBeRemoved = find(root, value);
        valueToBeRemoved.ifPresent(node -> {
            Node maxLeft;
            if (node.hasLeft()) {
                maxLeft = max(node.left);

                if (!maxLeft.hasLeft()){
                    if (node.parent != null){
                        node.parent.addChild(maxLeft);
                    }
                    maxLeft.parent.right = null;
                    maxLeft.left = node.left;
                    maxLeft.right = node.right;
                }else {
                    if (node.parent != null){
                        node.parent.addChild(maxLeft.left);
                    }
                    maxLeft.parent.right = null;
                    maxLeft.left = node.left;
                    maxLeft.right = node.right;
                }
            } else {
                node.parent.addChild(node.right);
            }

        });
    }

    public void clear() {
        root = null;
    }


    private class Node {
        private int value;
        private Node left;
        private Node right;
        private Node parent;

        public Node(int value) {
            this.value = value;
        }

        public Node(Node parent, int value) {
            this.value = value;
        }

        public void addChild(int value) {
            if (value < this.value) {
                this.left = new Node(this, value);
            } else {
                this.right = new Node(this, value);
            }
        }

        public boolean hasLeft() {
            return left != null;
        }

        public boolean hasRight() {
            return right != null;
        }

        public void addChild(Node node) {
            if(node.value<value){
                this.left = node;
            }else {
                this.right = node;
            }
        }
    }
}
