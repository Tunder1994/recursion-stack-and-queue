package pl.sda.javalub13.data.structures.queue;

public class SdaQueueImpl<T> implements SdaQueue<T> {

    Node<T> head = null;
    Node<T> tail = null;
    private int size = 0;

    @Override
    public T poll() {
        if (head != null) {
            size--;
            Node<T> oldHead = head;
            head = head.getNext();
            return oldHead.getData();
        } else {
            return null;
        }

    }

    @Override
    public void offer(T data) {
        Node<T> newNode = new Node<>(data);
        size++;
        if (head == null) {
            head = newNode;
            tail = newNode;
        } else {
            tail.setNext(newNode);
            tail = newNode;

        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    @Override
    public void remove(T data) {
        Node<T> current = head;
        if (head != null) {
            if (current.getData().equals(data)) {
                head = head.getNext();
                size--;
                return;
            } else {
                Node<T> next = current.getNext();
                while (next != null && !next.getData().equals(data)) {
                    current = current.getNext();
                    next = next.getNext();
                }
                if (next != null) {
                    current.setNext(next.getNext());
                    size--;
                }
            }
        }
    }

    @Override
    public T peek() {
        return head.getData();
    }

    public int size2() {
        int queueSize = 0;
        Node<T> current = head;
        while (current != null) {
            queueSize++;
            current = current.getNext();
        }
        return queueSize;
    }

    public int size3() {
        return sizeFrom(head);
    }

    private int sizeFrom(Node<T> startingPoint) {
        if (startingPoint != null) {
            return 1 + sizeFrom(startingPoint.next);
        } else {
            return 0;
        }
    }

    private class Node<T> {
        private T data;
        private Node<T> next;

        Node(T data) {
            this.data = data;
            next = null;
        }

        public void setNext(Node<T> next) {
            this.next = next;
        }

        public Node<T> getNext() {
            return next;
        }

        public T getData() {
            return data;
        }
    }
}
