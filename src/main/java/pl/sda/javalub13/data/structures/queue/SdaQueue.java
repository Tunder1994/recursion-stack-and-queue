package pl.sda.javalub13.data.structures.queue;

public interface SdaQueue<T> {
    T poll();

    void offer(T data);

    int size();

    void clear();

    void remove(T data);

    T peek();


}
